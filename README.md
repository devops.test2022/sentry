# sentry

Run docker-compose up -d

Run docker-compose exec sentry sentry upgrade to setup database and create admin user

(Optional) Run docker-compose exec sentry pip install sentry-slack if you want slack plugin, it can be done later

Run docker-compose restart sentry

Sentry is now running on public port 9000

Generate secret-key
docker-compose exec sentry sentry config generate-secret-key